import React from "react";
import './address.css'

function Address ({element}) {
    return(<>
        <article className='address'>
            <p className = 'title'>Адрес:</p>
            <p>{element.street}, {element.suite}, {element.city}, {element.zipcode}</p>
        </article>
    </>)
}

export default Address