import React from "react";
import "./user.css"

function User({element}){
    return(<>

        <label>
            <p className = 'label-style'>Ф.И.О</p>
            <input className = 'input-style' value={element.name} readOnly></input>
        </label>

        <label>
            <p className = 'label-style'>Никнейм</p>
            <input className = 'input-style' value={element.username} readOnly></input>
        </label>

        <label>
            <p className = 'label-style'>Email</p>
            <input className = 'input-style' value={element.email} readOnly></input>
        </label>

        <label>
            <p className = 'label-style'>Телефон</p>
            <input className = 'input-style' value={element.phone} readOnly></input>
        </label>

        <label>
            <p className = 'label-style'>Сайт</p>
            <input className = 'input-style' value={element.website} readOnly></input>
        </label>

        <p>
            <span className='sex'>Пол</span>
            <label className='sex-lable'>Мужской
                <input type='radio' name='sex' value='man'></input>
            </label>

            <label className='sex-lable'>Женский
                <input type='radio' name='sex' value='women'></input>
            </label>
        </p>

    </>)
}

export default User