import React from "react";
import UserCard from '../user-card'
import data from '../data/data'

function App() {
    return(<>
    <UserCard data = {data}/>
    </>)
}

export default App