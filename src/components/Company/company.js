import React from "react";
import './company.css'

function Company ({element}) {
    return(<>
        <article className='company'>
            <p className='title'>Компания:</p>
            <p className='parah-text'><span className='text'>Название:</span><span>{element.name}</span></p>
            <p className='parah-text'><span className='text'>Ключевые слова:</span><span>{element.catchPhrase}</span></p>
            <p className='parah-text'><span className='text'>Род бизнеса:</span><span>{element.bs}</span></p>
        </article>
    </>)
}

export default Company 