import React from "react";
import Address from '../Address/address'
import Company from '../Company/company'
import User from '../user/user'
import './user-card.css'

function UserCard(props) {
    return(<>
    <div className='body'>
    {props.data.map(function (element, index){
        return(
        <div className='user-card' key = {index}>
             
             { /* инпуты отдельной компонентой */ }
             <User element={element}/>

             { /* инпуты в начальном варианте */ }

            {/* <label>
                <p className = 'label-style'>Ф.И.О</p>
                <input className = 'input-style' value={element.name} readOnly></input>
            </label>

            <label>
                <p className = 'label-style'>Никнейм</p>
                <input className = 'input-style' value={element.username} readOnly></input>
            </label>

            <label>
                <p className = 'label-style'>Email</p>
                <input className = 'input-style' value={element.email} readOnly></input>
            </label>

            <label>
                <p className = 'label-style'>Телефон</p>
                <input className = 'input-style' value={element.phone} readOnly></input>
            </label>

            <label>
                <p className = 'label-style'>Сайт</p>
                <input className = 'input-style' value={element.website} readOnly></input>
            </label>

            <p>
                <span className='sex'>Пол</span>
                <label className='sex-lable'>Мужской
                    <input type='radio' name='sex' value='man'></input>
                </label>

                <label className='sex-lable'>Женский
                    <input type='radio' name='sex' value='women'></input>
                </label>
            </p> */}

            { /* создание болков адрес и компания с помощью двух доп. компонент */ }
            <section>
                <Address element={element.address}/>
                <Company element={element.company}/>
            </section>
            
        </div>)
    })}
    </div>
    </>)
}

export default UserCard